import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        System.out.println("Willkommen bei meinem Queue Test, bitte gib eines der Folgenden Kommandos ein: \n" +
                "- 'get queue'; wobei 'queue' 1 oder 2 ist, die entsorechende Queue \n" +
                "- 'put queue obj', wobei 'obj' die Zahl ist, die Hinzugefügt werden soll \n" +
                "- 'IsEmpty queue', gibt true zurück, wenn die entsprechende queue leer ist \n" +
                "beenden mit 'exit'");

        System.out.println("\n" +
                "Gib jetzt die Länge des Arrays an, das die beiden Queues speichert:");
         int size = sc.nextInt();
        ArrayQueues queues = new ArrayQueues(size);
        while (true){
            System.out.println("----------------------------");
            queues.printQueues();

               String line = sc.nextLine();
            if(line.equals("exit")){
                sc.close();
                break;
            }

            if(line.startsWith("get")){
                queues.Get(Integer.parseInt(line.substring(4,5)));
            } else if(line.startsWith("put")){
                String[] inputs = line.split(" ");
                queues.Put(Integer.parseInt(inputs[1]),Integer.parseInt(inputs[2]));
            } else if(line.toLowerCase().startsWith("isempty")){
                queues.IsEmpty(Integer.parseInt(line.substring(8,9)));
            }
            else {
                System.out.println("die Eingabe war nicht gültig, bitte versuche es erneut");
            }

        }
    }
}
