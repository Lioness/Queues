public class ArrayQueues{
    private Object[] array;
    private int head1, head2, tail1, tail2;

    public ArrayQueues(int n){
        array = new Object[n];
        head1 = 0;
        head2 = (n / 2);
        tail1 = head1;
        tail2 = head2;
    }

    Object Get(int queue){
        if(queue == 1){
            System.out.println("head1 position vorher: " + head1);
            // ist die queue leer, passiert nichts und null wird zurückgegeben
            if(array[head1] == null){
                return null;
            } else {
                // wenn die Queue voll war, und tail1 auf ein freies Feld zeigt,
                // wird jetzt tail1 auf das nächste (freiwerdende) Feld weiter gesetzt
                if(array[tail1] != null){
                    tail1 = head1;
                }
                // Das Objekt von Position head1 wird zwischengespeichert und die Position im Array auf null gesetzt
                Object result = array[head1];
                array[head1] = null;
                // head1 wird entsprechend auf die nächste position in seiner queue gesetzt
                if(head1 < (array.length / 2) - 1) {
                    head1++;
                } else {
                    head1 = 0;
                }
                System.out.println("new position of head1: " + head1);
                return result;
            }
        } else {
            System.out.println("head2 position vorher: " + head2 + "; value: " + array[head2]);
            if(array[head2] == null){
                return null;
            } else {
                if(array[tail2] != null){
                    tail2 = head2;
                }

                Object result = array[head2];
                array[head2] = null;
                if(head2 == array.length - 1){
                    head2 = array.length / 2;
                } else {
                    head2 ++;
                }
                System.out.println("new position of head2: " + head2);
                return result;
            }
        }
    }

    void Put(int queue, Object obj){


        if(queue == 1){
            System.out.println("tail1 position vorher: " + tail1);
            // existiert ein freier platz, wird dieser mit obj gefüllt
            if(array[tail1] == null){
                array[tail1] = obj;

                //tail1 wird entsprechend weiter gesetzt
                if(tail1 < array.length / 2 - 1){
                    tail1++;
                }else {
                    tail1 = 0;
                }
            } else {
                System.out.println("es gibt keinen freien Platz mehr");
            }


            System.out.println("tail1 position nachher: " + tail1);
        } else {
            System.out.println("tail2 position vorher: " + tail2);
            if(array[tail2] == null){
                array[tail2] = obj;

                if(tail2 < array.length-1){
                    tail2++;
                } else {
                    tail2 = array.length / 2;
                }
            } else {
                System.out.println("es gibt keinen freien Platz mehr");
            }
            System.out.println("tail2 position nachher: " + tail2);
        }
    }

    boolean IsEmpty(int queue){
        if(queue == 1){
            for (int i = 0; i < array.length / 2; i++) {
                if(array[i] != null){
                    return false;
                }
            }
            return true;
        } else {
            for (int i = array.length / 2; i < array.length; i++) {
                if(array[i] != null){
                    return false;
                }
            }
            return true;
        }
    }

    // nur zur Ausgabe
    void printQueues(){
        String values = "|";
        String position = "|";
        String puffer = "";
        for (int i = 0; i < array.length; i++) {
            if(i == array.length / 2){
                values += " - |";
            }

            if(array[i] == null){
                puffer = "   ";
            }else if((int) array[i] < 10){
                puffer = "  " + array[i];
            }else if((int) array[i] < 100){
                puffer = " " + array[i];
            }else if((int) array[i] < 1000){
                puffer = "" + array[i];
            }
            values += puffer + "|";
        }

        System.out.println(values);

        for (int i = 0; i < array.length; i++) {
            if(i == array.length / 2){
                position += " - |";
            }

            if(i < 10){
                puffer = "  ";
            }else if(i < 100){
                puffer = " ";
            }else if(i < 1000){
                puffer = "";
            }
            position += puffer + i + "|";
        }
        position += " (positions)";

        System.out.println(position);
    }
}
